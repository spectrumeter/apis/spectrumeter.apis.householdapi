# SpectruMeter.Apis.HouseholdApi
The SpectruMeter.Apis.HouseholdApi handles all requests for a househould. Changing the values or actions like adding facility or a resident to the household.

## Requirements
The run the SpectruMeter.Apis.HouseholdApi you need:
- PostgreSQL

## Configuration
In appsettings.json

```json
  "ConnectionStrings": {
    "Default": "Host=localhost; Database=householdapi; Username=postgres; Password=password"
  },
```

## Open Api
If you run SpectruMeter.Apis.HouseholdApi in debug mode a swagger will be exposed to http://address:port/swagger
