using SpectruMeter.Apis.HouseholdApi.Models;

namespace SpectruMeter.Apis.HouseholdApi.Services.Interfaces
{
    public interface IFacilityService
    {
        Task<FacilityDto[]> GetAsync(CancellationToken cancellationToken = default);
        Task<FacilityGroupDto[]> GetGroupsAsync(CancellationToken cancellationToken = default);
    }
}
