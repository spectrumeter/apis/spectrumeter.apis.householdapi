using SpectruMeter.Apis.HouseholdApi.Models;

namespace SpectruMeter.Apis.HouseholdApi.Services.Interfaces
{
    public interface IHouseholdService
    {
        Task<HouseholdDto> GetAsync(Guid id, CancellationToken cancellationToken = default);
        Task<HouseholdDto> GetByUserIdAsync(
            string userId,
            CancellationToken cancellationToken = default
        );

        Task DeleteAsync(Guid id, CancellationToken cancellationToken = default);

        Task<HouseholdDto> CreateAsync(
            HouseholdCreateDto household,
            HouseTypeDto houseType,
            List<ResidentCreateDto>? residents,
            List<FacilityDto>? facilities,
            string userId,
            CancellationToken cancellationToken = default
        );

        Task UpdateAsync(
            HouseholdUpdateDto household,
            CancellationToken cancellationToken = default
        );

        Task<HouseholdDto> AddFacilityAsync(
            Guid householdId,
            Guid facilityId,
            CancellationToken cancellationToken = default
        );

        Task<HouseholdDto> DeleteFacilityAsync(
            Guid householdId,
            Guid facilityId,
            CancellationToken cancellationToken = default
        );

        Task<HouseholdDto> AddResidentAsync(
            Guid householdId,
            ResidentCreateDto resident,
            CancellationToken cancellationToken = default
        );

        Task<HouseholdDto> DeleteResidentAsync(
            Guid householdId,
            Guid residentId,
            CancellationToken cancellationToken = default
        );

        Task<HouseholdDto> UpdateHouseTypeAsync(
            Guid householdId,
            Guid houseTypeId,
            CancellationToken cancellationToken = default
        );

        Task<List<HouseholdDto>> SearchAsync(
            double latitude,
            double longitude,
            double distance,
            DateTimeOffset minBirthdayYear,
            DateTimeOffset maxBirthdayYear,
            List<Guid> facilities,
            List<Guid> houseTypes,
            CancellationToken cancellationToken = default
        );
    }
}
