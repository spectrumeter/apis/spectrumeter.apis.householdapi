using SpectruMeter.Apis.HouseholdApi.Models;

namespace SpectruMeter.Apis.HouseholdApi.Services.Interfaces
{
    public interface IHouseTypeService
    {
        Task<HouseTypeDto[]> GetAsync(CancellationToken cancellationToken = default);
    }
}
