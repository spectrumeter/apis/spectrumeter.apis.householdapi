using Mapster;
using Microsoft.EntityFrameworkCore;
using SpectruMeter.Apis.HouseholdApi.DAL.Interfaces;
using SpectruMeter.Apis.HouseholdApi.Models;
using SpectruMeter.Apis.HouseholdApi.Services.Interfaces;

namespace SpectruMeter.Apis.HouseholdApi.Services
{
    public class HouseTypeService : IHouseTypeService
    {
        private readonly ILogger<HouseTypeService> _logger;
        private readonly IHouseTypeRepository _houseTypeRepository;

        public HouseTypeService(
            ILogger<HouseTypeService> logger,
            IHouseTypeRepository houseTypeRepository
        )
        {
            this._logger = logger;
            this._houseTypeRepository = houseTypeRepository;
        }

        public async Task<HouseTypeDto[]> GetAsync(CancellationToken cancellationToken = default)
        {
            var result = this._houseTypeRepository.GetQuery();

            return (await result.ToArrayAsync()).Adapt<HouseTypeDto[]>();
        }
    }
}
