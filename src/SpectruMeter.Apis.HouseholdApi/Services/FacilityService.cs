using Mapster;
using Microsoft.EntityFrameworkCore;
using SpectruMeter.Apis.HouseholdApi.DAL.Interfaces;
using SpectruMeter.Apis.HouseholdApi.Models;
using SpectruMeter.Apis.HouseholdApi.Services.Interfaces;

namespace SpectruMeter.Apis.HouseholdApi.Services
{
    public class FacilityService : IFacilityService
    {
        private readonly ILogger<FacilityService> _logger;
        private readonly IFacilityRepository _facilityRepository;

        public FacilityService(
            ILogger<FacilityService> logger,
            IFacilityRepository facilityRepository
        )
        {
            this._logger = logger;
            this._facilityRepository = facilityRepository;
        }

        public async Task<FacilityDto[]> GetAsync(CancellationToken cancellationToken = default)
        {
            var results = this._facilityRepository.GetQuery();

            return (await results.ToArrayAsync()).Adapt<FacilityDto[]>();
        }

        public async Task<FacilityGroupDto[]> GetGroupsAsync(
            CancellationToken cancellationToken = default
        )
        {
            var results = this._facilityRepository.GetGroupQuery().Include(o => o.Facilities);

            return (await results.ToArrayAsync()).Adapt<FacilityGroupDto[]>();
        }
    }
}
