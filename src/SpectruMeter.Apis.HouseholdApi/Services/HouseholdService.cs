using Mapster;
using Microsoft.EntityFrameworkCore;
using RabbitMQ.Client;
using SpectruMeter.Apis.HouseholdApi.DAL.Interfaces;
using SpectruMeter.Apis.HouseholdApi.DAL.Models;
using SpectruMeter.Apis.HouseholdApi.Exceptions;
using SpectruMeter.Apis.HouseholdApi.Models;
using SpectruMeter.Apis.HouseholdApi.Services.Interfaces;
using System.Text;
using System.Text.Json;

namespace SpectruMeter.Apis.HouseholdApi.Services
{
    public class HouseholdService : IHouseholdService
    {
        private const string HOUSEHOLD_EXCHANGE = "HouseholdExchange";
        private readonly ILogger<HouseholdService> _logger;
        private readonly IConfiguration _configuration;
        private readonly IHouseholdRepository _householdRepository;
        private readonly IResidentRepository _residentRepository;
        private readonly IHouseTypeRepository _houseTypeRepository;
        private readonly IFacilityRepository _facilityRepository;
        private static IConnection _connection;

        public HouseholdService(
            ILogger<HouseholdService> logger,
            IConfiguration configuration,
            IHouseholdRepository householdRepository,
            IResidentRepository residentRepository,
            IHouseTypeRepository houseTypeRepository,
            IFacilityRepository facilityRepository
        )
        {
            this._logger = logger;
            this._configuration = configuration;
            this._householdRepository = householdRepository;
            this._residentRepository = residentRepository;
            this._houseTypeRepository = houseTypeRepository;
            this._facilityRepository = facilityRepository;
        }

        public async Task<HouseholdDto> AddFacilityAsync(
            Guid householdId,
            Guid facilityId,
            CancellationToken cancellationToken = default
        )
        {
            var dbHousehold = this._householdRepository
                .GetQuery(o => o.Id == householdId)
                .Include(o => o.Facilities)
                .Include(o => o.Residents)
                .Include(o => o.HouseType)
                .FirstOrDefault();

            if (dbHousehold == null)
            {
                throw new NotFoundException($"Household with Id: {householdId} not found");
            }

            var dbFacility = this._facilityRepository
                .GetQuery(o => o.Id == facilityId)
                .FirstOrDefault();
            if (dbFacility == null)
            {
                throw new NotFoundException($"Facility with Id: {facilityId} not found");
            }

            await this._householdRepository.UpdateAsync(
                o => o.Id == householdId,
                o => o.Facilities.Add(dbFacility),
                cancellationToken
            );

            return dbHousehold.Adapt<HouseholdDto>();
        }

        public async Task<HouseholdDto> AddResidentAsync(
            Guid householdId,
            ResidentCreateDto resident,
            CancellationToken cancellationToken = default
        )
        {
            var dbHousehold = this._householdRepository
                .GetQuery(o => o.Id == householdId)
                .Include(o => o.Facilities)
                .Include(o => o.Residents)
                .Include(o => o.HouseType)
                .FirstOrDefault();

            if (dbHousehold == null)
            {
                throw new NotFoundException($"Household with Id: {householdId} not found");
            }

            var dbResident = resident.Adapt<Resident>();
            await this._residentRepository.AddAsync(dbResident, cancellationToken);

            await this._householdRepository.UpdateAsync(
                o => o.Id == householdId,
                o => o.Residents.Add(dbResident),
                cancellationToken
            );

            return dbHousehold.Adapt<HouseholdDto>();
        }

        public async Task<HouseholdDto> CreateAsync(
            HouseholdCreateDto household,
            HouseTypeDto houseType,
            List<ResidentCreateDto>? residents,
            List<FacilityDto>? facilities,
            string userId,
            CancellationToken cancellationToken = default
        )
        {
            var dbHoushold = household.Adapt<Household>();
            dbHoushold.Location = new NetTopologySuite.Geometries.Point(
                household.Longitude,
                household.Latitude
            );

            var dbHouseType = this._houseTypeRepository
                .GetQuery(o => o.Id == houseType.Id)
                .FirstOrDefault();
            if (dbHouseType == null)
            {
                throw new NotFoundException($"HouseType with Id: {houseType.Id} not found");
            }

            dbHoushold.HouseType = dbHouseType;
            dbHoushold.HouseTypeId = dbHouseType.Id;
            dbHoushold.UserId = userId;

            foreach (var facility in facilities ?? new List<FacilityDto>())
            {
                var dbFacility = this._facilityRepository
                    .GetQuery(o => o.Id == facility.Id)
                    .FirstOrDefault();
                if (dbFacility == null)
                {
                    throw new NotFoundException($"Facility with Id: {facility.Id} not found");
                }

                dbHoushold.Facilities.Add(dbFacility);
            }

            foreach (var resident in residents ?? new List<ResidentCreateDto>())
            {
                var residentToSave = resident.Adapt<Resident>();
                residentToSave.Birthday = new DateTimeOffset(
                    resident.Birthday.Year,
                    1,
                    1,
                    0,
                    0,
                    0,
                    TimeSpan.Zero
                );
                var dbResident = await this._residentRepository.AddAsync(
                    residentToSave,
                    cancellationToken
                );
                dbHoushold.Residents.Add(dbResident);
            }

            var resultHousehold = await this._householdRepository.AddAsync(dbHoushold);

            var result = resultHousehold.Adapt<HouseholdDto>();
            try
            {
                using var channel = this.GetChannel();
                var body = Encoding.UTF8.GetBytes(JsonSerializer.Serialize(result));

                channel.BasicPublish(
                    exchange: HOUSEHOLD_EXCHANGE,
                    routingKey: "household-created",
                    basicProperties: null,
                    body: body
                );
            }
            catch (Exception ex)
            {
                this._logger.LogError(ex, "Error while publishing data to MQ");
            }

            return result;
        }

        public async Task<HouseholdDto> DeleteFacilityAsync(
            Guid householdId,
            Guid facilityId,
            CancellationToken cancellationToken = default
        )
        {
            var dbHousehold = this._householdRepository
                .GetQuery(o => o.Id == householdId)
                .Include(o => o.Facilities)
                .Include(o => o.Residents)
                .Include(o => o.HouseType)
                .FirstOrDefault();

            if (dbHousehold == null)
            {
                throw new NotFoundException($"Household with Id: {householdId} not found");
            }

            var dbFacility = this._facilityRepository
                .GetQuery(o => o.Id == facilityId)
                .FirstOrDefault();
            if (dbFacility == null)
            {
                throw new NotFoundException($"Facility with Id: {facilityId} not found");
            }

            await this._householdRepository.UpdateAsync(
                o => o.Id == householdId,
                o => o.Facilities.Remove(dbFacility),
                cancellationToken
            );

            return dbHousehold.Adapt<HouseholdDto>();
        }

        public async Task<HouseholdDto> DeleteResidentAsync(
            Guid householdId,
            Guid residentId,
            CancellationToken cancellationToken = default
        )
        {
            var dbHousehold = this._householdRepository
                .GetQuery(o => o.Id == householdId)
                .Include(o => o.Facilities)
                .Include(o => o.Residents)
                .Include(o => o.HouseType)
                .FirstOrDefault();

            if (dbHousehold == null)
            {
                throw new NotFoundException($"Household with Id: {householdId} not found");
            }

            var dbResident = this._residentRepository
                .GetQuery(o => o.Id == residentId)
                .FirstOrDefault();
            if (dbResident == null)
            {
                throw new NotFoundException($"Resident with Id: {residentId} not found");
            }

            await this._householdRepository.UpdateAsync(
                o => o.Id == householdId,
                o => o.Residents.Remove(dbResident),
                cancellationToken
            );

            return dbHousehold.Adapt<HouseholdDto>();
        }

        public async Task<HouseholdDto> GetAsync(
            Guid id,
            CancellationToken cancellationToken = default
        )
        {
            var dbHousehold = this._householdRepository
                .GetQuery(o => o.Id == id)
                .Include(o => o.Facilities)
                .Include(o => o.Residents)
                .Include(o => o.HouseType)
                .FirstOrDefault();

            if (dbHousehold == null)
            {
                throw new NotFoundException($"Household with Id: {id} not found");
            }

            return dbHousehold.Adapt<HouseholdDto>();
        }

        public async Task<HouseholdDto> UpdateHouseTypeAsync(
            Guid householdId,
            Guid houseTypeId,
            CancellationToken cancellationToken = default
        )
        {
            var dbHousehold = this._householdRepository
                .GetQuery(o => o.Id == householdId)
                .Include(o => o.Facilities)
                .Include(o => o.Residents)
                .Include(o => o.HouseType)
                .FirstOrDefault();

            if (dbHousehold == null)
            {
                throw new NotFoundException($"Household with Id: {householdId} not found");
            }

            var dbHouseType = this._houseTypeRepository
                .GetQuery(o => o.Id == houseTypeId)
                .FirstOrDefault();
            if (dbHouseType == null)
            {
                throw new NotFoundException($"HouseType with Id: {houseTypeId} not found");
            }

            await this._householdRepository.UpdateAsync(
                o => o.Id == householdId,
                o =>
                {
                    o.HouseType = dbHouseType;
                    o.HouseTypeId = dbHouseType.Id;
                },
                cancellationToken
            );

            return dbHousehold.Adapt<HouseholdDto>();
        }

        private IModel GetChannel()
        {
            if (_connection == null || !_connection.IsOpen)
            {
                var factory = new ConnectionFactory()
                {
                    HostName = this._configuration["RabbitMQ:Url"]
                };
                _connection = factory.CreateConnection();
                this._logger.LogInformation("Open RabbitMq Connection");
            }

            var channel = _connection.CreateModel();

            channel.ExchangeDeclare(exchange: HOUSEHOLD_EXCHANGE, type: "direct");

            return channel;
        }

        public async Task<HouseholdDto> GetByUserIdAsync(
            string userId,
            CancellationToken cancellationToken = default
        )
        {
            var dbHousehold = this._householdRepository
                .GetQuery(o => o.UserId == userId)
                .Include(o => o.Facilities)
                .Include(o => o.Residents)
                .Include(o => o.HouseType)
                .OrderBy(o => o.CreatedAt)
                .LastOrDefault();

            if (dbHousehold == null)
            {
                throw new NotFoundException($"Household with UserId: {userId} not found");
            }

            return dbHousehold.Adapt<HouseholdDto>();
        }

        public Task<List<HouseholdDto>> SearchAsync(
            double latitude,
            double longitude,
            double distance,
            DateTimeOffset minBirthdayYear,
            DateTimeOffset maxBirthdayYear,
            List<Guid> facilities,
            List<Guid> houseTypes,
            CancellationToken cancellationToken = default
        )
        {
            var dbHousehold = this._householdRepository.GetQuery(
                o =>
                    o.Location.IsWithinDistance(
                        NetTopologySuite.Geometries.Geometry.DefaultFactory.CreatePoint(
                            new NetTopologySuite.Geometries.Coordinate(longitude, latitude)
                        ),
                        distance
                    )
                    && o.Residents.Any(
                        r => r.Birthday >= minBirthdayYear && r.Birthday <= maxBirthdayYear
                    )
            //&& facilities.Any(f => o.Facilities.Any(fac => fac.Id == f))
            //&& houseTypes.Any(h => o.HouseTypeId == h)
            );
            if (facilities.Any())
            {
                dbHousehold = dbHousehold.Where(
                    o => o.Facilities.Any(f => facilities.Contains(f.Id))
                );
            }

            if (houseTypes.Any())
            {
                dbHousehold = dbHousehold.Where(o => houseTypes.Contains(o.HouseTypeId));
            }

            dbHousehold
                .Include(o => o.Facilities)
                .Include(o => o.Residents)
                .Include(o => o.HouseType)
                .ToList();

            return Task.FromResult(dbHousehold.Adapt<List<HouseholdDto>>());
        }

        public async Task UpdateAsync(
            HouseholdUpdateDto household,
            CancellationToken cancellationToken = default
        )
        {
            await this._householdRepository.UpdateAsync(
                o => o.Id == household.HouseholdId,
                o =>
                {
                    o.Name = household.Name;
                    o.Location = new NetTopologySuite.Geometries.Point(
                        household.Longitude,
                        household.Latitude
                    );
                    o.Longitude = household.Longitude;
                    o.Latitude = household.Latitude;
                    o.DeviceId = household.DeviceId;
                    o.AcceptContractAutomatically = household.AcceptContractAutomatically;
                },
                cancellationToken
            );

            try
            {
                using var channel = this.GetChannel();
                var body = Encoding.UTF8.GetBytes(JsonSerializer.Serialize(household));

                channel.BasicPublish(
                    exchange: HOUSEHOLD_EXCHANGE,
                    routingKey: "household-updated",
                    basicProperties: null,
                    body: body
                );
            }
            catch (Exception ex)
            {
                this._logger.LogError(ex, "Error while publishing data to MQ");
            }
        }

        public async Task DeleteAsync(Guid id, CancellationToken cancellationToken = default)
        {
            await this._householdRepository.DeleteAsync(id, cancellationToken);
        }
    }
}
