using Microsoft.AspNetCore.Mvc;
using SpectruMeter.Apis.HouseholdApi.Models;
using SpectruMeter.Apis.HouseholdApi.Services.Interfaces;

namespace SpectruMeter.Apis.HouseholdApi.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class HouseTypeController : ControllerBase
    {
        private readonly ILogger<HouseTypeController> _logger;
        private readonly IHouseTypeService _houseTypeService;

        public HouseTypeController(
            ILogger<HouseTypeController> logger,
            IHouseTypeService houseTypeService
        )
        {
            this._logger = logger;
            this._houseTypeService = houseTypeService;
        }

        [HttpGet]
        public async Task<ActionResult<HouseTypeResponseDto>> Get()
        {
            var result = await this._houseTypeService.GetAsync();
            return this.StatusCode(
                StatusCodes.Status200OK,
                new HouseTypeResponseDto { HouseTypes = result }
            );
        }
    }
}
