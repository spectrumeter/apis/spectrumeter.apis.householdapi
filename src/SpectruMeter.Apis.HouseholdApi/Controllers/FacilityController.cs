using Microsoft.AspNetCore.Mvc;
using SpectruMeter.Apis.HouseholdApi.Models;
using SpectruMeter.Apis.HouseholdApi.Services.Interfaces;

namespace SpectruMeter.Apis.HouseholdApi.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class FacilityController : ControllerBase
    {
        private readonly ILogger<FacilityController> _logger;
        private readonly IFacilityService _facilityService;

        public FacilityController(
            ILogger<FacilityController> logger,
            IFacilityService facilityService
        )
        {
            this._logger = logger;
            this._facilityService = facilityService;
        }

        [HttpGet]
        public async Task<ActionResult<FacilityResponseDto>> Get()
        {
            var result = await this._facilityService.GetAsync();
            return this.StatusCode(
                StatusCodes.Status200OK,
                new FacilityResponseDto { Facilities = result }
            );
        }

        [HttpGet]
        [Route("Groups")]
        public async Task<ActionResult<FacilityGroupsResponseDto>> GetGroups()
        {
            var result = await this._facilityService.GetGroupsAsync();
            return this.StatusCode(
                StatusCodes.Status200OK,
                new FacilityGroupsResponseDto { FacilityGroups = result }
            );
        }
    }
}
