using Microsoft.AspNetCore.Mvc;
using SpectruMeter.Apis.HouseholdApi.Exceptions;
using SpectruMeter.Apis.HouseholdApi.Models;
using SpectruMeter.Apis.HouseholdApi.Services.Interfaces;

namespace SpectruMeter.Apis.HouseholdApi.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class HouseholdController : ControllerBase
    {
        private readonly ILogger<HouseholdController> _logger;
        private readonly IHouseholdService _householdService;
        private readonly IHouseTypeService _houseTypeService;

        public HouseholdController(
            ILogger<HouseholdController> logger,
            IHouseholdService householdService,
            IHouseTypeService houseTypeService
        )
        {
            this._logger = logger;
            this._householdService = householdService;
            this._houseTypeService = houseTypeService;
        }

        [HttpPost]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound, Type = typeof(NotFoundResponseDto))]
        [Route("{userId}")]
        public async Task<ActionResult<HouseholdCreateResponseDto>> Create(
            [FromBody] HouseholdCreateRequestDto data,
            [FromRoute] string userId
        )
        {
            try
            {
                if (data.HouseType == null)
                {
                    var types = await this._houseTypeService.GetAsync();
                    data.HouseType = types.FirstOrDefault();
                }

                var result = await this._householdService.CreateAsync(
                    data.Household,
                    data.HouseType,
                    data.Residents,
                    data.Facilities,
                    userId
                );
                return this.StatusCode(
                    StatusCodes.Status200OK,
                    new HouseholdCreateResponseDto { Household = result }
                );
            }
            catch (NotFoundException ex)
            {
                return this.StatusCode(
                    StatusCodes.Status404NotFound,
                    new NotFoundResponseDto { Message = ex.Message }
                );
            }
        }

        [HttpPut]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound, Type = typeof(NotFoundResponseDto))]
        public async Task<IActionResult> Update([FromBody] HouseholdUpdateDto data)
        {
            try
            {
                await this._householdService.UpdateAsync(data);
                return this.StatusCode(StatusCodes.Status200OK);
            }
            catch (NotFoundException ex)
            {
                return this.StatusCode(
                    StatusCodes.Status404NotFound,
                    new NotFoundResponseDto { Message = ex.Message }
                );
            }
        }

        [HttpPost]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound, Type = typeof(NotFoundResponseDto))]
        [Route("{houseHoldId}/Facility/{facilityId}")]
        public async Task<ActionResult<HouseholdAddFacilityResponseDto>> AddFacility(
            [FromRoute] Guid houseHoldId,
            [FromRoute] Guid facilityId
        )
        {
            try
            {
                var result = await this._householdService.AddFacilityAsync(houseHoldId, facilityId);
                return this.StatusCode(
                    StatusCodes.Status200OK,
                    new HouseholdAddFacilityResponseDto { Household = result }
                );
            }
            catch (NotFoundException ex)
            {
                return this.StatusCode(
                    StatusCodes.Status404NotFound,
                    new NotFoundResponseDto { Message = ex.Message }
                );
            }
        }

        [HttpDelete]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound, Type = typeof(NotFoundResponseDto))]
        [Route("{houseHoldId}/Facility/{facilityId}")]
        public async Task<ActionResult<HouseholdDeleteFacilityResponseDto>> DeleteFacility(
            [FromRoute] Guid houseHoldId,
            [FromRoute] Guid facilityId
        )
        {
            try
            {
                var result = await this._householdService.DeleteFacilityAsync(
                    houseHoldId,
                    facilityId
                );
                return this.StatusCode(
                    StatusCodes.Status200OK,
                    new HouseholdDeleteFacilityResponseDto { Household = result }
                );
            }
            catch (NotFoundException ex)
            {
                return this.StatusCode(
                    StatusCodes.Status404NotFound,
                    new NotFoundResponseDto { Message = ex.Message }
                );
            }
        }

        [HttpPost]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound, Type = typeof(NotFoundResponseDto))]
        [Route("{houseHoldId}/Resident")]
        public async Task<ActionResult<HousehouldAddResidentResponseDto>> AddResident(
            [FromRoute] Guid houseHoldId,
            [FromBody] HousehouldAddResidentRequestDto request
        )
        {
            try
            {
                var result = await this._householdService.AddResidentAsync(
                    houseHoldId,
                    request.Resident
                );
                return this.StatusCode(
                    StatusCodes.Status200OK,
                    new HousehouldAddResidentResponseDto { Household = result }
                );
            }
            catch (NotFoundException ex)
            {
                return this.StatusCode(
                    StatusCodes.Status404NotFound,
                    new NotFoundResponseDto { Message = ex.Message }
                );
            }
        }

        [HttpDelete]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound, Type = typeof(NotFoundResponseDto))]
        [Route("{houseHoldId}/Resident/{residentId}")]
        public async Task<ActionResult<HouseholdDeleteResidentResponseDto>> DeleteResident(
            [FromRoute] Guid houseHoldId,
            [FromRoute] Guid residentId
        )
        {
            try
            {
                var result = await this._householdService.DeleteResidentAsync(
                    houseHoldId,
                    residentId
                );
                return this.StatusCode(
                    StatusCodes.Status200OK,
                    new HouseholdDeleteResidentResponseDto { Household = result }
                );
            }
            catch (NotFoundException ex)
            {
                return this.StatusCode(
                    StatusCodes.Status404NotFound,
                    new NotFoundResponseDto { Message = ex.Message }
                );
            }
        }

        [HttpPut]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound, Type = typeof(NotFoundResponseDto))]
        [Route("{houseHoldId}/HouseType/{houseTypeId}")]
        public async Task<ActionResult<HouseholdUpdateHouseTypeResponseDto>> UpdateHouseType(
            [FromRoute] Guid houseHoldId,
            [FromRoute] Guid houseTypeId
        )
        {
            try
            {
                var result = await this._householdService.UpdateHouseTypeAsync(
                    houseHoldId,
                    houseTypeId
                );
                return this.StatusCode(
                    StatusCodes.Status200OK,
                    new HouseholdUpdateHouseTypeResponseDto { Household = result }
                );
            }
            catch (NotFoundException ex)
            {
                return this.StatusCode(
                    StatusCodes.Status404NotFound,
                    new NotFoundResponseDto { Message = ex.Message }
                );
            }
        }

        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound, Type = typeof(NotFoundResponseDto))]
        [Route("{houseHoldId}")]
        public async Task<ActionResult<HouseholdResponseDto>> Get([FromRoute] Guid houseHoldId)
        {
            try
            {
                var result = await this._householdService.GetAsync(houseHoldId);
                return this.StatusCode(
                    StatusCodes.Status200OK,
                    new HouseholdResponseDto { Household = result }
                );
            }
            catch (NotFoundException ex)
            {
                return this.StatusCode(
                    StatusCodes.Status404NotFound,
                    new NotFoundResponseDto { Message = ex.Message }
                );
            }
        }

        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound, Type = typeof(NotFoundResponseDto))]
        [Route("User/{userId}")]
        public async Task<ActionResult<HouseholdResponseDto>> GetByUserId([FromRoute] string userId)
        {
            try
            {
                var result = await this._householdService.GetByUserIdAsync(userId);
                return this.StatusCode(
                    StatusCodes.Status200OK,
                    new HouseholdResponseDto { Household = result }
                );
            }
            catch (NotFoundException ex)
            {
                return this.StatusCode(
                    StatusCodes.Status404NotFound,
                    new NotFoundResponseDto { Message = ex.Message }
                );
            }
        }

        [HttpPost]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [Route("Search")]
        public async Task<ActionResult<HouseholdSearchResponseDto>> Search(
            [FromBody] HouseholdSearchRequestDto data
        )
        {
            try
            {
                var result = await this._householdService.SearchAsync(
                    data.Latitude,
                    data.Longitude,
                    data.Distance,
                    data.MinBirthdayYear,
                    data.MaxBirthdayYear,
                    data.Facilities,
                    data.HouseTypes
                );
                return this.StatusCode(
                    StatusCodes.Status200OK,
                    new HouseholdSearchResponseDto { Households = result }
                );
            }
            catch (NotFoundException ex)
            {
                return this.StatusCode(
                    StatusCodes.Status404NotFound,
                    new NotFoundResponseDto { Message = ex.Message }
                );
            }
        }

        [HttpDelete]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [Route("{householdId}")]
        public async Task<ActionResult<HouseholdSearchResponseDto>> Delete(
            [FromRoute] Guid householdId
        )
        {
            try
            {
                await this._householdService.DeleteAsync(householdId);
                return this.StatusCode(StatusCodes.Status200OK);
            }
            catch (NotFoundException ex)
            {
                return this.StatusCode(
                    StatusCodes.Status404NotFound,
                    new NotFoundResponseDto { Message = ex.Message }
                );
            }
        }
    }
}
