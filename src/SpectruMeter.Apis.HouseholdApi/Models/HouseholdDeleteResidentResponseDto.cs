namespace SpectruMeter.Apis.HouseholdApi.Models
{
    public class HouseholdDeleteResidentResponseDto
    {
        public HouseholdDto Household { get; set; }
    }
}
