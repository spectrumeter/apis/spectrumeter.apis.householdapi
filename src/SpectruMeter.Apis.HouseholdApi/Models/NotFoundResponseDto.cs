namespace SpectruMeter.Apis.HouseholdApi.Models
{
    public class NotFoundResponseDto
    {
        public string Message { get; set; }
    }
}
