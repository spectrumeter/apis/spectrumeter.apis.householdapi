namespace SpectruMeter.Apis.HouseholdApi.Models
{
    public class FacilityResponseDto
    {
        public FacilityDto[] Facilities { get; set; }
    }
}
