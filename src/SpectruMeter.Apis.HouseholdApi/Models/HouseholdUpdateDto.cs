namespace SpectruMeter.Apis.HouseholdApi.Models
{
    public class HouseholdUpdateDto
    {
        public Guid HouseholdId { get; set; }
        public string? DeviceId { get; set; }
        public string Name { get; set; }
        public double Latitude { get; set; }
        public double Longitude { get; set; }
        public bool AcceptContractAutomatically { get; set; }
    }
}
