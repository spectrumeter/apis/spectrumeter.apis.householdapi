namespace SpectruMeter.Apis.HouseholdApi.Models
{
    public class FacilityDto
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string? Manufacturer { get; set; }

        /// <summary>
        /// Power Consumption in Watt
        /// </summary>
        public decimal PowerConsumption { get; set; }
    }
}
