namespace SpectruMeter.Apis.HouseholdApi.Models
{
    public class HouseholdDeleteFacilityResponseDto
    {
        public HouseholdDto Household { get; set; }
    }
}
