namespace SpectruMeter.Apis.HouseholdApi.Models
{
    public class HouseholdCreateResponseDto
    {
        public HouseholdDto Household { get; set; }
    }
}
