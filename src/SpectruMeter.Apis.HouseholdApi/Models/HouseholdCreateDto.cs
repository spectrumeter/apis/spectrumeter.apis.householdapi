namespace SpectruMeter.Apis.HouseholdApi.Models
{
    public class HouseholdCreateDto
    {
        public string UserId { get; set; }
        public string? DeviceId { get; set; }
        public string Name { get; set; }
        public double Latitude { get; set; }
        public double Longitude { get; set; }

        //public int Rooms { get; set; }
        //public decimal SqaureMeters { get; set; }
        public bool AcceptContractAutomatically { get; set; }
    }
}
