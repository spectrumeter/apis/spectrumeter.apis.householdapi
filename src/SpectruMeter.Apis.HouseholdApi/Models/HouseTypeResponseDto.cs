namespace SpectruMeter.Apis.HouseholdApi.Models
{
    public class HouseTypeResponseDto
    {
        public HouseTypeDto[] HouseTypes { get; set; }
    }
}
