namespace SpectruMeter.Apis.HouseholdApi.Models
{
    public class HouseholdResponseDto
    {
        public HouseholdDto Household { get; set; }
    }
}
