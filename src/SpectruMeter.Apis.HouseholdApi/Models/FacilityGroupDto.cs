namespace SpectruMeter.Apis.HouseholdApi.Models
{
    public class FacilityGroupDto
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public ICollection<FacilityDto> Facilities { get; set; } = new List<FacilityDto>();
    }
}
