namespace SpectruMeter.Apis.HouseholdApi.Models
{
    public class ResidentCreateDto
    {
        public DateTimeOffset Birthday { get; set; }
    }
}
