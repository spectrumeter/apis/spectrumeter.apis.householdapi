namespace SpectruMeter.Apis.HouseholdApi.Models
{
    public class HousehouldAddResidentResponseDto
    {
        public HouseholdDto Household { get; set; }
    }
}
