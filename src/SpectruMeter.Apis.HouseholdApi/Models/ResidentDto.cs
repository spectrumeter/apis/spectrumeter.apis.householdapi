namespace SpectruMeter.Apis.HouseholdApi.Models
{
    public class ResidentDto
    {
        public Guid Id { get; set; }
        public DateTimeOffset Birthday { get; set; }
    }
}
