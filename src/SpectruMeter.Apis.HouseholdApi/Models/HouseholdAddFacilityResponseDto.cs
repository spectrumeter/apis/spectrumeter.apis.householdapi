namespace SpectruMeter.Apis.HouseholdApi.Models
{
    public class HouseholdAddFacilityResponseDto
    {
        public HouseholdDto Household { get; set; }
    }
}
