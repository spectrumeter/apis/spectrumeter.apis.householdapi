namespace SpectruMeter.Apis.HouseholdApi.Models
{
    public class HouseholdSearchResponseDto
    {
        public List<HouseholdDto> Households { get; set; }
    }
}
