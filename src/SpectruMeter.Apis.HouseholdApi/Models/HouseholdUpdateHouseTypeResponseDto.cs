namespace SpectruMeter.Apis.HouseholdApi.Models
{
    public class HouseholdUpdateHouseTypeResponseDto
    {
        public HouseholdDto Household { get; set; }
    }
}
