namespace SpectruMeter.Apis.HouseholdApi.Models
{
    public class HouseholdDto
    {
        public Guid Id { get; set; }
        public string UserId { get; set; }
        public string? DeviceId { get; set; }
        public string Name { get; set; }
        public double Latitude { get; set; }
        public double Longitude { get; set; }
        public bool AcceptContractAutomatically { get; set; }
        public DateTime CreatedAt { get; set; }
        public HouseTypeDto HouseType { get; set; }
        public Guid HouseTypeId { get; set; }
        public ICollection<FacilityDto> Facilities { get; set; } = new List<FacilityDto>();
        public ICollection<ResidentDto> Residents { get; set; } = new List<ResidentDto>();
    }
}
