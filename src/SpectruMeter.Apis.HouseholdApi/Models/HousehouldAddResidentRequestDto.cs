namespace SpectruMeter.Apis.HouseholdApi.Models
{
    public class HousehouldAddResidentRequestDto
    {
        public ResidentCreateDto Resident { get; set; }
    }
}
