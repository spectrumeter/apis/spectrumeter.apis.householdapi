namespace SpectruMeter.Apis.HouseholdApi.Models
{
    public class FacilityGroupsResponseDto
    {
        public FacilityGroupDto[] FacilityGroups { get; set; }
    }
}
