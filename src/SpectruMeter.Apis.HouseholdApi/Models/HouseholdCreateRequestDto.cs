namespace SpectruMeter.Apis.HouseholdApi.Models
{
    public class HouseholdCreateRequestDto
    {
        public HouseholdCreateDto Household { get; set; }
        public List<ResidentCreateDto> Residents { get; set; }
        public HouseTypeDto? HouseType { get; set; }
        public List<FacilityDto> Facilities { get; set; }
    }
}
