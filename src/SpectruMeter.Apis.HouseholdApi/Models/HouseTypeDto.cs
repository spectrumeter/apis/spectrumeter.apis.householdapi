namespace SpectruMeter.Apis.HouseholdApi.Models
{
    public class HouseTypeDto
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
    }
}
