namespace SpectruMeter.Apis.HouseholdApi.Models
{
    public class HouseholdSearchRequestDto
    {
        public double Latitude { get; set; }
        public double Longitude { get; set; }
        public double Distance { get; set; }
        public DateTimeOffset MinBirthdayYear { get; set; }
        public DateTimeOffset MaxBirthdayYear { get; set; }
        public List<Guid> Facilities { get; set; }
        public List<Guid> HouseTypes { get; set; }
    }
}
