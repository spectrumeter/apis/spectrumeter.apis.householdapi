using Microsoft.AspNetCore.Mvc.Controllers;
using Microsoft.EntityFrameworkCore;
using Npgsql;
using SpectruMeter.Apis.HouseholdApi.DAL;
using SpectruMeter.Apis.HouseholdApi.DAL.Interfaces;
using SpectruMeter.Apis.HouseholdApi.Helpers;
using SpectruMeter.Apis.HouseholdApi.Services;
using SpectruMeter.Apis.HouseholdApi.Services.Interfaces;

var builder = WebApplication.CreateBuilder(args);
var configuration = builder.Configuration;

AppContext.SetSwitch("Npgsql.EnableLegacyTimestampBehavior", true);

// Add Database
builder.Services.AddDbContext<DatabaseContext>(o =>
{
    o.UseNpgsql(configuration["ConnectionStrings:Default"], x => x.UseNetTopologySuite())
        .LogTo(Console.WriteLine, LogLevel.Warning)
        .EnableDetailedErrors();
});

// Add services to the container.
builder.Services.AddScoped<IHouseholdService, HouseholdService>();
builder.Services.AddScoped<IFacilityService, FacilityService>();
builder.Services.AddScoped<IHouseTypeService, HouseTypeService>();

builder.Services.AddScoped<IFacilityRepository, SqlFacilityRepository>();
builder.Services.AddScoped<IHouseholdRepository, SqlHouseholdRepository>();
builder.Services.AddScoped<IHouseTypeRepository, SqlHouseTypeRepository>();
builder.Services.AddScoped<IResidentRepository, SqlResidentRepository>();

builder.Services.AddScoped<InitHelper>();

builder.Services.AddControllers();

// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen(configuration =>
{
    configuration.CustomOperationIds(description =>
    {
        var actionDescriptor = description.ActionDescriptor as ControllerActionDescriptor;
        return $"{actionDescriptor.ControllerName}{actionDescriptor.ActionName}";
    });
});

var app = builder.Build();
var logger = app.Services.GetService<ILogger<Program>>();

UpdateDatabaseAsync(logger, app).GetAwaiter().GetResult();
InitDefaultValues(logger, app).GetAwaiter().GetResult();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseAuthorization();

app.MapControllers();

app.Run();

async Task UpdateDatabaseAsync(ILogger<Program> logger, IApplicationBuilder app)
{
    var retryCount = 60;

    for (var i = 0; i < retryCount; i++)
    {
        using (
            var serviceScope = app.ApplicationServices
                .GetRequiredService<IServiceScopeFactory>()
                .CreateScope()
        )
        {
            using (var context = serviceScope.ServiceProvider.GetService<DatabaseContext>())
            {
                try
                {
                    context.Database.Migrate();
                    context.Database.OpenConnection();
                    ((NpgsqlConnection)context.Database.GetDbConnection()).ReloadTypes();
                    break;
                }
                catch (Exception exception)
                {
                    logger.LogError(
                        exception,
                        $"{nameof(UpdateDatabaseAsync)} - Cannot execute database migrate, retry: {i}"
                    );
                    await Task.Delay(1000);
                }
            }
        }
    }
}

async Task InitDefaultValues(ILogger<Program> logger, IApplicationBuilder app)
{
    using (var serviceScope = app.ApplicationServices.CreateScope())
    {
        var services = serviceScope.ServiceProvider;

        var initHelper = services.GetRequiredService<InitHelper>();
        initHelper.SetupFacilities().GetAwaiter().GetResult();
        initHelper.SetupHouseTypes().GetAwaiter().GetResult();
    }
}
