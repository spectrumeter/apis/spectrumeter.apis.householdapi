using SpectruMeter.Apis.HouseholdApi.DAL.Interfaces;
using SpectruMeter.Apis.HouseholdApi.DAL.Models;

namespace SpectruMeter.Apis.HouseholdApi.Helpers
{
    public class InitHelper
    {
        private readonly string[] _facilities = new string[]
        {
            "PV",
            "Heat Pump",
            "Coffee Machine"
        };

        private readonly FacilityGroup[] _facilityGroups = new FacilityGroup[]
        {
            new FacilityGroup
            {
                Name = "PV",
                Facilities = new Facility[]
                {
                    new Facility { Name = "BOSCH PV" },
                    new Facility { Name = "KA PV" },
                    new Facility { Name = "mo PV" }
                }
            },
            new FacilityGroup
            {
                Name = "Heat Pump",
                Facilities = new Facility[]
                {
                    new Facility { Name = "Super Pump" },
                    new Facility { Name = "Glump Pump" }
                }
            },
            new FacilityGroup
            {
                Name = "Coffee Machine",
                Facilities = new Facility[]
                {
                    new Facility { Name = "AEG Machine" },
                    new Facility { Name = "BOSCH Machine" }
                }
            }
        };

        private readonly string[] _houseTypes = new string[] { "House", "DoubleHouse", "Flat" };

        private readonly ILogger<InitHelper> _logger;
        private readonly IFacilityRepository _facilityRepository;
        private readonly IHouseTypeRepository _houseTypeRepository;

        public InitHelper(
            ILogger<InitHelper> logger,
            IFacilityRepository facilityRepository,
            IHouseTypeRepository houseTypeRepository
        )
        {
            this._logger = logger;
            this._facilityRepository = facilityRepository;
            this._houseTypeRepository = houseTypeRepository;
        }

        public async Task SetupFacilities()
        {
            var facilityGroups = this._facilityRepository.GetGroupQuery().ToArray();
            if (facilityGroups.Length == 0)
            {
                foreach (var facilityGroup in this._facilityGroups)
                {
                    await this._facilityRepository.AddGroupAsync(facilityGroup);
                }
            }
        }

        public async Task SetupHouseTypes()
        {
            var houseTypes = this._houseTypeRepository.GetQuery().ToArray();
            if (houseTypes.Length == 0)
            {
                foreach (var houseType in this._houseTypes)
                {
                    var dbHouseType = new HouseType { Name = houseType };

                    await this._houseTypeRepository.AddAsync(dbHouseType);
                }
            }
        }
    }
}
