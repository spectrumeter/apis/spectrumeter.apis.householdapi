using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using SpectruMeter.Apis.HouseholdApi.DAL.Interfaces;
using SpectruMeter.Apis.HouseholdApi.DAL.Models;
using System.Linq.Expressions;

namespace SpectruMeter.Apis.HouseholdApi.DAL
{
    public class SqlHouseholdRepository : IHouseholdRepository
    {
        private readonly ILogger<SqlHouseholdRepository> _logger;
        private readonly DatabaseContext _databaseContext;

        public SqlHouseholdRepository(
            ILogger<SqlHouseholdRepository> logger,
            DatabaseContext databaseContext
        )
        {
            this._logger = logger;
            this._databaseContext = databaseContext;
        }

        public async Task<Household> AddAsync(
            Household household,
            CancellationToken cancellationToken = default
        )
        {
            await this._databaseContext.AddAsync(household, cancellationToken);
            await this._databaseContext.SaveChangesAsync(cancellationToken);

            return household;
        }

        public async Task<bool> DeleteAsync(Guid id, CancellationToken cancellationToken = default)
        {
            var household = await this._databaseContext.Households.FirstOrDefaultAsync(
                o => o.Id == id,
                cancellationToken
            );
            if (household == null)
            {
                return false;
            }

            this._databaseContext.Households.Remove(household);
            await this._databaseContext.SaveChangesAsync(cancellationToken);

            return true;
        }

        public IQueryable<Household> GetQuery(Expression<Func<Household, bool>>? predicate = null)
        {
            if (predicate == null)
            {
                predicate = o => true;
            }
            return this._databaseContext.Households.Where(predicate);
        }

        public async Task<bool> UpdateAsync(
            Expression<Func<Household, bool>> predicate,
            Action<Household> updateAction,
            CancellationToken cancellationToken = default
        )
        {
            var household = await this._databaseContext.Households
                .Where(predicate)
                .ToArrayAsync(cancellationToken);

            foreach (var item in household)
            {
                updateAction(item);
            }

            await this._databaseContext.SaveChangesAsync(cancellationToken);

            return true;
        }
    }
}
