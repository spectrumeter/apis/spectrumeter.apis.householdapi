using Microsoft.EntityFrameworkCore;
using SpectruMeter.Apis.HouseholdApi.DAL.Models;

namespace SpectruMeter.Apis.HouseholdApi.DAL
{
    public class DatabaseContext : DbContext
    {
        private readonly DbContextOptions<DatabaseContext> _options;
        public DbSet<Facility> Facilities { get; set; }
        public DbSet<FacilityGroup> FacilityGroups { get; set; }
        public DbSet<Household> Households { get; set; }
        public DbSet<Resident> Residents { get; set; }
        public DbSet<HouseType> HouseTypes { get; set; }

        public DatabaseContext() { }

        public DatabaseContext(DbContextOptions<DatabaseContext> options)
            : base(options)
        {
            this._options = options;
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseNpgsql(
                    "Data Source=(localdb)\\MSSQLLocalDB;Initial Catalog=master;Integrated Security=True;Connect Timeout=30;Encrypt=False;Trust Server Certificate=False;Application Intent=ReadWrite;Multi Subnet Failover=False",
                    x => x.UseNetTopologySuite()
                );
            }
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.Entity<Household>().HasIndex(o => o.UserId);
            builder.Entity<Household>().HasIndex(o => o.DeviceId);
        }
    }
}
