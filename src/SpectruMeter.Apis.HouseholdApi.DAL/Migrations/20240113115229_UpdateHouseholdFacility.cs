﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace SpectruMeter.Apis.HouseholdApi.DAL.Migrations
{
    /// <inheritdoc />
    public partial class UpdateHouseholdFacility : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "City",
                table: "Households");

            migrationBuilder.DropColumn(
                name: "Country",
                table: "Households");

            migrationBuilder.DropColumn(
                name: "Number",
                table: "Households");

            migrationBuilder.DropColumn(
                name: "Street",
                table: "Households");

            migrationBuilder.AddColumn<bool>(
                name: "AcceptContractAutomatically",
                table: "Households",
                type: "boolean",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<string>(
                name: "Manufacturer",
                table: "Facilities",
                type: "character varying(50)",
                maxLength: 50,
                nullable: true);

            migrationBuilder.AddColumn<decimal>(
                name: "PowerConsumption",
                table: "Facilities",
                type: "numeric",
                nullable: false,
                defaultValue: 0m);
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "AcceptContractAutomatically",
                table: "Households");

            migrationBuilder.DropColumn(
                name: "Manufacturer",
                table: "Facilities");

            migrationBuilder.DropColumn(
                name: "PowerConsumption",
                table: "Facilities");

            migrationBuilder.AddColumn<string>(
                name: "City",
                table: "Households",
                type: "character varying(100)",
                maxLength: 100,
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<string>(
                name: "Country",
                table: "Households",
                type: "character varying(100)",
                maxLength: 100,
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<string>(
                name: "Number",
                table: "Households",
                type: "character varying(30)",
                maxLength: 30,
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<string>(
                name: "Street",
                table: "Households",
                type: "text",
                nullable: false,
                defaultValue: "");
        }
    }
}
