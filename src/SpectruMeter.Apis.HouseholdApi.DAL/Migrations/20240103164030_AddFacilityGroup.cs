﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace SpectruMeter.Apis.HouseholdApi.DAL.Migrations
{
    /// <inheritdoc />
    public partial class AddFacilityGroup : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<Guid>(
                name: "FacilityGroupId",
                table: "Facilities",
                type: "uuid",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.CreateTable(
                name: "FacilityGroups",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uuid", nullable: false),
                    Name = table.Column<string>(type: "character varying(50)", maxLength: 50, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_FacilityGroups", x => x.Id);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Facilities_FacilityGroupId",
                table: "Facilities",
                column: "FacilityGroupId");

            migrationBuilder.AddForeignKey(
                name: "FK_Facilities_FacilityGroups_FacilityGroupId",
                table: "Facilities",
                column: "FacilityGroupId",
                principalTable: "FacilityGroups",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Facilities_FacilityGroups_FacilityGroupId",
                table: "Facilities");

            migrationBuilder.DropTable(
                name: "FacilityGroups");

            migrationBuilder.DropIndex(
                name: "IX_Facilities_FacilityGroupId",
                table: "Facilities");

            migrationBuilder.DropColumn(
                name: "FacilityGroupId",
                table: "Facilities");
        }
    }
}
