﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace SpectruMeter.Apis.HouseholdApi.DAL.Migrations
{
    /// <inheritdoc />
    public partial class RemoveUselessFields : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Rooms",
                table: "Households");

            migrationBuilder.DropColumn(
                name: "SqaureMeters",
                table: "Households");

            migrationBuilder.DropColumn(
                name: "Test",
                table: "Households");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "Rooms",
                table: "Households",
                type: "integer",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<decimal>(
                name: "SqaureMeters",
                table: "Households",
                type: "numeric",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<int>(
                name: "Test",
                table: "Households",
                type: "integer",
                nullable: false,
                defaultValue: 0);
        }
    }
}
