﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace SpectruMeter.Apis.HouseholdApi.DAL.Migrations
{
    /// <inheritdoc />
    public partial class Init : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Facilities",
                columns: table =>
                    new
                    {
                        Id = table.Column<Guid>(type: "uuid", nullable: false),
                        Name = table.Column<string>(
                            type: "character varying(50)",
                            maxLength: 50,
                            nullable: false
                        )
                    },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Facilities", x => x.Id);
                }
            );

            migrationBuilder.CreateTable(
                name: "HouseTypes",
                columns: table =>
                    new
                    {
                        Id = table.Column<Guid>(type: "uuid", nullable: false),
                        Name = table.Column<string>(
                            type: "character varying(50)",
                            maxLength: 50,
                            nullable: false
                        )
                    },
                constraints: table =>
                {
                    table.PrimaryKey("PK_HouseTypes", x => x.Id);
                }
            );

            migrationBuilder.CreateTable(
                name: "Households",
                columns: table =>
                    new
                    {
                        Id = table.Column<Guid>(type: "uuid", nullable: false),
                        UserId = table.Column<string>(
                            type: "character varying(50)",
                            maxLength: 50,
                            nullable: false
                        ),
                        DeviceId = table.Column<string>(
                            type: "character varying(50)",
                            maxLength: 50,
                            nullable: true
                        ),
                        Name = table.Column<string>(
                            type: "character varying(50)",
                            maxLength: 50,
                            nullable: false
                        ),
                        Street = table.Column<string>(type: "text", nullable: false),
                        Number = table.Column<string>(
                            type: "character varying(30)",
                            maxLength: 30,
                            nullable: false
                        ),
                        City = table.Column<string>(
                            type: "character varying(100)",
                            maxLength: 100,
                            nullable: false
                        ),
                        Country = table.Column<string>(
                            type: "character varying(100)",
                            maxLength: 100,
                            nullable: false
                        ),
                        Latitude = table.Column<double>(type: "double precision", nullable: false),
                        Longitude = table.Column<double>(type: "double precision", nullable: false),
                        Rooms = table.Column<int>(type: "integer", nullable: false),
                        SqaureMeters = table.Column<decimal>(type: "numeric", nullable: false),
                        HouseTypeId = table.Column<Guid>(type: "uuid", nullable: false),
                        CreatedAt = table.Column<DateTime>(
                            type: "timestamp with time zone",
                            nullable: false
                        )
                    },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Households", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Households_HouseTypes_HouseTypeId",
                        column: x => x.HouseTypeId,
                        principalTable: "HouseTypes",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade
                    );
                }
            );

            migrationBuilder.CreateTable(
                name: "FacilityHousehold",
                columns: table =>
                    new
                    {
                        FacilitiesId = table.Column<Guid>(type: "uuid", nullable: false),
                        HouseholdsId = table.Column<Guid>(type: "uuid", nullable: false)
                    },
                constraints: table =>
                {
                    table.PrimaryKey(
                        "PK_FacilityHousehold",
                        x => new { x.FacilitiesId, x.HouseholdsId }
                    );
                    table.ForeignKey(
                        name: "FK_FacilityHousehold_Facilities_FacilitiesId",
                        column: x => x.FacilitiesId,
                        principalTable: "Facilities",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade
                    );
                    table.ForeignKey(
                        name: "FK_FacilityHousehold_Households_HouseholdsId",
                        column: x => x.HouseholdsId,
                        principalTable: "Households",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade
                    );
                }
            );

            migrationBuilder.CreateTable(
                name: "Residents",
                columns: table =>
                    new
                    {
                        Id = table.Column<Guid>(type: "uuid", nullable: false),
                        Birthday = table.Column<DateTimeOffset>(
                            type: "timestamp with time zone",
                            nullable: false
                        ),
                        HouseholdId = table.Column<Guid>(type: "uuid", nullable: true)
                    },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Residents", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Residents_Households_HouseholdId",
                        column: x => x.HouseholdId,
                        principalTable: "Households",
                        principalColumn: "Id"
                    );
                }
            );

            migrationBuilder.CreateIndex(
                name: "IX_FacilityHousehold_HouseholdsId",
                table: "FacilityHousehold",
                column: "HouseholdsId"
            );

            migrationBuilder.CreateIndex(
                name: "IX_Households_DeviceId",
                table: "Households",
                column: "DeviceId"
            );

            migrationBuilder.CreateIndex(
                name: "IX_Households_HouseTypeId",
                table: "Households",
                column: "HouseTypeId"
            );

            migrationBuilder.CreateIndex(
                name: "IX_Households_UserId",
                table: "Households",
                column: "UserId"
            );

            migrationBuilder.CreateIndex(
                name: "IX_Residents_HouseholdId",
                table: "Residents",
                column: "HouseholdId"
            );
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(name: "FacilityHousehold");

            migrationBuilder.DropTable(name: "Residents");

            migrationBuilder.DropTable(name: "Facilities");

            migrationBuilder.DropTable(name: "Households");

            migrationBuilder.DropTable(name: "HouseTypes");
        }
    }
}
