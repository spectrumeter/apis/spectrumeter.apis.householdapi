using SpectruMeter.Apis.HouseholdApi.DAL.Models;
using System.Linq.Expressions;

namespace SpectruMeter.Apis.HouseholdApi.DAL.Interfaces
{
    public interface IResidentRepository
    {
        Task<Resident> AddAsync(Resident resident, CancellationToken cancellationToken = default);

        IQueryable<Resident> GetQuery(Expression<Func<Resident, bool>>? predicate = default);

        Task<bool> UpdateAsync(
            Expression<Func<Resident, bool>> predicate,
            Action<Resident> updateAction,
            CancellationToken cancellationToken = default
        );

        Task<bool> DeleteAsync(Guid id, CancellationToken cancellationToken = default);
    }
}
