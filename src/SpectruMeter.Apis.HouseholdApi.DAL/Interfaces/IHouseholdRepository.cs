using SpectruMeter.Apis.HouseholdApi.DAL.Models;
using System.Linq.Expressions;

namespace SpectruMeter.Apis.HouseholdApi.DAL.Interfaces
{
    public interface IHouseholdRepository
    {
        Task<Household> AddAsync(
            Household household,
            CancellationToken cancellationToken = default
        );

        IQueryable<Household> GetQuery(Expression<Func<Household, bool>>? predicate = default);

        Task<bool> UpdateAsync(
            Expression<Func<Household, bool>> predicate,
            Action<Household> updateAction,
            CancellationToken cancellationToken = default
        );

        Task<bool> DeleteAsync(Guid id, CancellationToken cancellationToken = default);
    }
}
