using SpectruMeter.Apis.HouseholdApi.DAL.Models;
using System.Linq.Expressions;

namespace SpectruMeter.Apis.HouseholdApi.DAL.Interfaces
{
    public interface IFacilityRepository
    {
        Task<Facility> AddAsync(Facility facility, CancellationToken cancellationToken = default);
        Task<FacilityGroup> AddGroupAsync(
            FacilityGroup facility,
            CancellationToken cancellationToken = default
        );

        IQueryable<Facility> GetQuery(Expression<Func<Facility, bool>>? predicate = default);
        IQueryable<FacilityGroup> GetGroupQuery(
            Expression<Func<FacilityGroup, bool>>? predicate = default
        );

        Task<bool> UpdateAsync(
            Expression<Func<Facility, bool>> predicate,
            Action<Facility> updateAction,
            CancellationToken cancellationToken = default
        );

        Task<bool> DeleteAsync(Guid id, CancellationToken cancellationToken = default);
    }
}
