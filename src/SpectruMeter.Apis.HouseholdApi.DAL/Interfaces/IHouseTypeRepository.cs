using SpectruMeter.Apis.HouseholdApi.DAL.Models;
using System.Linq.Expressions;

namespace SpectruMeter.Apis.HouseholdApi.DAL.Interfaces
{
    public interface IHouseTypeRepository
    {
        Task<HouseType> AddAsync(
            HouseType houseType,
            CancellationToken cancellationToken = default
        );

        IQueryable<HouseType> GetQuery(Expression<Func<HouseType, bool>>? predicate = default);

        Task<bool> UpdateAsync(
            Expression<Func<HouseType, bool>> predicate,
            Action<HouseType> updateAction,
            CancellationToken cancellationToken = default
        );

        Task<bool> DeleteAsync(Guid id, CancellationToken cancellationToken = default);
    }
}
