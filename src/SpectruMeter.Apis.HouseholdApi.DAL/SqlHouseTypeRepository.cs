using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using SpectruMeter.Apis.HouseholdApi.DAL.Interfaces;
using SpectruMeter.Apis.HouseholdApi.DAL.Models;
using System.Linq.Expressions;

namespace SpectruMeter.Apis.HouseholdApi.DAL
{
    public class SqlHouseTypeRepository : IHouseTypeRepository
    {
        private readonly ILogger<SqlHouseTypeRepository> _logger;
        private readonly DatabaseContext _databaseContext;

        public SqlHouseTypeRepository(
            ILogger<SqlHouseTypeRepository> logger,
            DatabaseContext databaseContext
        )
        {
            this._logger = logger;
            this._databaseContext = databaseContext;
        }

        public async Task<HouseType> AddAsync(
            HouseType houseType,
            CancellationToken cancellationToken = default
        )
        {
            await this._databaseContext.AddAsync(houseType, cancellationToken);
            await this._databaseContext.SaveChangesAsync(cancellationToken);

            return houseType;
        }

        public async Task<bool> DeleteAsync(Guid id, CancellationToken cancellationToken = default)
        {
            var household = await this._databaseContext.HouseTypes.FirstOrDefaultAsync(
                o => o.Id == id,
                cancellationToken
            );
            if (household == null)
            {
                return false;
            }

            this._databaseContext.HouseTypes.Remove(household);
            await this._databaseContext.SaveChangesAsync(cancellationToken);

            return true;
        }

        public IQueryable<HouseType> GetQuery(Expression<Func<HouseType, bool>>? predicate = null)
        {
            if (predicate == null)
            {
                predicate = o => true;
            }
            return this._databaseContext.HouseTypes.Where(predicate);
        }

        public async Task<bool> UpdateAsync(
            Expression<Func<HouseType, bool>> predicate,
            Action<HouseType> updateAction,
            CancellationToken cancellationToken = default
        )
        {
            var household = await this._databaseContext.HouseTypes
                .Where(predicate)
                .ToArrayAsync(cancellationToken);

            foreach (var item in household)
            {
                updateAction(item);
            }

            await this._databaseContext.SaveChangesAsync(cancellationToken);

            return true;
        }
    }
}
