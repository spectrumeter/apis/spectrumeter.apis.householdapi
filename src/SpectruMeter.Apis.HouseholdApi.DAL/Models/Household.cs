using NetTopologySuite.Geometries;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SpectruMeter.Apis.HouseholdApi.DAL.Models
{
    public class Household
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid Id { get; set; }

        [StringLength(50)]
        public string UserId { get; set; }

        [StringLength(50)]
        public string? DeviceId { get; set; }

        [StringLength(50)]
        public string Name { get; set; }
        public double Latitude { get; set; }
        public double Longitude { get; set; }

        //public int Rooms { get; set; }
        //public decimal SqaureMeters { get; set; }
        public bool AcceptContractAutomatically { get; set; }

        //public int Test { get; set; }

        public Point Location { get; set; }

        [ForeignKey(nameof(HouseTypeId))]
        public HouseType HouseType { get; set; }
        public Guid HouseTypeId { get; set; }
        public ICollection<Facility> Facilities { get; set; } = new List<Facility>();
        public ICollection<Resident> Residents { get; set; } = new List<Resident>();
        public DateTime CreatedAt { get; set; } = DateTime.Now;
    }
}
