﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SpectruMeter.Apis.HouseholdApi.DAL.Models
{
    public class HouseType
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid Id { get; set; }

        [StringLength(50)]
        public string Name { get; set; }
        public ICollection<Household> Households { get; set; } = new List<Household>();
    }
}
