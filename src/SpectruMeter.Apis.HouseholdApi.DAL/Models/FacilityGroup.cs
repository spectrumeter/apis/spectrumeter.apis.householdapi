using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SpectruMeter.Apis.HouseholdApi.DAL.Models
{
    public class FacilityGroup
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid Id { get; set; }

        [StringLength(50)]
        public string Name { get; set; }
        public ICollection<Facility> Facilities { get; set; } = new List<Facility>();
    }
}
