using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SpectruMeter.Apis.HouseholdApi.DAL.Models
{
    public class Resident
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid Id { get; set; }
        public DateTimeOffset Birthday { get; set; }

        [ForeignKey(nameof(HouseholdId))]
        public Household? Household { get; set; }
        public Guid? HouseholdId { get; set; }
    }
}
