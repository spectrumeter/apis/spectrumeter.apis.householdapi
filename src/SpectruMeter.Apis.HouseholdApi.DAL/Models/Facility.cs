using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SpectruMeter.Apis.HouseholdApi.DAL.Models
{
    public class Facility
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid Id { get; set; }

        [StringLength(50)]
        public string Name { get; set; }

        [StringLength(50)]
        public string? Manufacturer { get; set; }

        /// <summary>
        /// Power Consumption in Watt
        /// </summary>
        public decimal PowerConsumption { get; set; }

        [ForeignKey(nameof(FacilityGroupId))]
        public FacilityGroup FacilityGroup { get; set; }
        public Guid FacilityGroupId { get; set; }
        public ICollection<Household> Households { get; set; } = new List<Household>();
    }
}
