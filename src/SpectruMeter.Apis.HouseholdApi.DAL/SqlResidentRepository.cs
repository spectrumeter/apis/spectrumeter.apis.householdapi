using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using SpectruMeter.Apis.HouseholdApi.DAL.Interfaces;
using SpectruMeter.Apis.HouseholdApi.DAL.Models;
using System.Linq.Expressions;

namespace SpectruMeter.Apis.HouseholdApi.DAL
{
    public class SqlResidentRepository : IResidentRepository
    {
        private readonly ILogger<SqlResidentRepository> _logger;
        private readonly DatabaseContext _databaseContext;

        public SqlResidentRepository(
            ILogger<SqlResidentRepository> logger,
            DatabaseContext databaseContext
        )
        {
            this._logger = logger;
            this._databaseContext = databaseContext;
        }

        public async Task<Resident> AddAsync(
            Resident resident,
            CancellationToken cancellationToken = default
        )
        {
            await this._databaseContext.AddAsync(resident, cancellationToken);
            await this._databaseContext.SaveChangesAsync(cancellationToken);

            return resident;
        }

        public async Task<bool> DeleteAsync(Guid id, CancellationToken cancellationToken = default)
        {
            var resident = await this._databaseContext.Residents.FirstOrDefaultAsync(
                o => o.Id == id,
                cancellationToken
            );
            if (resident == null)
            {
                return false;
            }

            this._databaseContext.Residents.Remove(resident);
            await this._databaseContext.SaveChangesAsync(cancellationToken);

            return true;
        }

        public IQueryable<Resident> GetQuery(Expression<Func<Resident, bool>>? predicate = null)
        {
            if (predicate == null)
            {
                predicate = o => true;
            }
            return this._databaseContext.Residents.Where(predicate);
        }

        public async Task<bool> UpdateAsync(
            Expression<Func<Resident, bool>> predicate,
            Action<Resident> updateAction,
            CancellationToken cancellationToken = default
        )
        {
            var resident = await this._databaseContext.Residents
                .Where(predicate)
                .ToArrayAsync(cancellationToken);

            foreach (var item in resident)
            {
                updateAction(item);
            }

            await this._databaseContext.SaveChangesAsync(cancellationToken);

            return true;
        }
    }
}
