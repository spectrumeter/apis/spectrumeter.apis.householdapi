using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using SpectruMeter.Apis.HouseholdApi.DAL.Interfaces;
using SpectruMeter.Apis.HouseholdApi.DAL.Models;
using System.Linq.Expressions;

namespace SpectruMeter.Apis.HouseholdApi.DAL
{
    public class SqlFacilityRepository : IFacilityRepository
    {
        private readonly ILogger<SqlFacilityRepository> _logger;
        private readonly DatabaseContext _databaseContext;

        public SqlFacilityRepository(
            ILogger<SqlFacilityRepository> logger,
            DatabaseContext databaseContext
        )
        {
            this._logger = logger;
            this._databaseContext = databaseContext;
        }

        public async Task<Facility> AddAsync(
            Facility facility,
            CancellationToken cancellationToken = default
        )
        {
            await this._databaseContext.AddAsync(facility, cancellationToken);
            await this._databaseContext.SaveChangesAsync(cancellationToken);

            return facility;
        }

        public async Task<FacilityGroup> AddGroupAsync(
            FacilityGroup facilityGroup,
            CancellationToken cancellationToken = default
        )
        {
            await this._databaseContext.AddAsync(facilityGroup, cancellationToken);
            await this._databaseContext.SaveChangesAsync(cancellationToken);

            return facilityGroup;
        }

        public async Task<bool> DeleteAsync(Guid id, CancellationToken cancellationToken = default)
        {
            var facility = await this._databaseContext.Facilities.FirstOrDefaultAsync(
                o => o.Id == id,
                cancellationToken
            );
            if (facility == null)
            {
                return false;
            }

            this._databaseContext.Facilities.Remove(facility);
            await this._databaseContext.SaveChangesAsync(cancellationToken);

            return true;
        }

        public IQueryable<FacilityGroup> GetGroupQuery(
            Expression<Func<FacilityGroup, bool>>? predicate = null
        )
        {
            if (predicate == null)
            {
                predicate = o => true;
            }

            return this._databaseContext.FacilityGroups.Where(predicate);
        }

        public IQueryable<Facility> GetQuery(Expression<Func<Facility, bool>>? predicate = null)
        {
            if (predicate == null)
            {
                predicate = o => true;
            }

            return this._databaseContext.Facilities.Where(predicate);
        }

        public async Task<bool> UpdateAsync(
            Expression<Func<Facility, bool>> predicate,
            Action<Facility> updateAction,
            CancellationToken cancellationToken = default
        )
        {
            var facility = await this._databaseContext.Facilities
                .Where(predicate)
                .ToArrayAsync(cancellationToken);

            foreach (var item in facility)
            {
                updateAction(item);
            }

            await this._databaseContext.SaveChangesAsync(cancellationToken);

            return true;
        }
    }
}
